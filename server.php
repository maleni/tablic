<?php

	include 'klasa.php';

	session_start();

	function sendJSONandExit( $message )
	{
		// Kao izlaz skripte pošalji $message u JSON formatu i prekini izvođenje.
		header( 'Content-type:application/json;charset=utf-8' );
		echo json_encode( $message );
		flush();
		exit( 0 );
	}

	Tablic::inicijalizirajOkruzenje();

	if($_SESSION['admin'])
	{
		if( !$_SESSION['igra']->pokrenuto() )
		{
			$_SESSION['igra']->pokreniPartiju();
		}
	}
	else
	{
		while( !$_SESSION['igra']->pokrenuto() )
		{
			// nista
		}
		$_SESSION['vrijeme'] = $_SESSION['igra']->dohvatiVrijeme();
	}

	if (array_key_exists('pokreni', $_GET))
		sendJSONandExit($_SESSION['igra']->prikaziStanje($_SESSION['id']));
	else
		sendJSONandExit($_SESSION['igra']->sljedeceStanje($_SESSION['id']));

?>
