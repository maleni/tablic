<?php
	function jednakiSkupovi ($S, $T)
	{
		while (!(empty($S) || empty($T)))
		{
			$x = array_pop($S);
			$y = array_pop($T);
			if ($x == $y)
				continue;
			$j = array_search($x, $T);
			$i = array_search($y, $S);
			if ($i === FALSE || $j === FALSE)
				return FALSE;
			array_splice($S, $i, 1);
			array_splice($T, $j, 1);
		}

		if (empty($S) && empty($T))
			return TRUE;

		return FALSE;
	}

	function disjunktniSkupovi ($S, $T)
	{
		while (!(empty($S) || empty($T)))
		{
			$x = array_pop($S);
			$y = array_pop($T);
			if ($x == $y)
				return FALSE;
			$j = array_search($x, $T);
			$i = array_search($y, $S);
			if ($i !== FALSE || $j !== FALSE)
				return FALSE;
		}

		if (empty($S) || empty($T))
			return TRUE;

		return FALSE;
	}

	function unijaSkupova ($S, $T)
	{
		foreach ($T as $y)
			array_push($S, $y);
		
		return $S;
	}

	function partitivniSkup ($S)
	{
		$P = array(array());

		foreach ($S as $x)
		{
			$Q = array();
			foreach ($P as $A)
			{
				array_push($A, $x);
				array_push($Q, $A);
			}
			while (!empty($Q))
				array_push($P, array_pop($Q));
		}

		return $P;
	}

	function unijeDisjunktnih ($F)
	{
		$U = array(array());

		foreach ($F as $S)
		{
			$V = array();
			foreach ($U as $A)
				if (disjunktniSkupovi($S, $A))
					array_push($V, unijaSkupova($S, $A));
			while (!empty($V))
				array_push($U, array_pop($V));
		}

		return $U;
	}

	class Tablic
	{
		private static $db;

		private static $nedefinirano = 'X';

		private static $labeleBoja = array('H' => 1, 'S' => 2, 'D' => 3, 'C' => 4);
		private static $antilabeleBoja = array(1 => 'H', 2 => 'S', 3 => 'D', 4 => 'C');

		private static $labeleZnakova = array('A' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9, '10' => 10, 'J' => 12, 'Q' => 13, 'K' => 14);
		private static $antilabeleZnakova = array(1 => 'A', 2 => '2', 3 => '3', 4 => '4', 5 => '5', 6 => '6', 7 => '7', 8 => '8', 9 => '9', 10 => '10', 11 => 'A', 12 => 'J', 13 => 'Q', 14 => 'K');

		private $id;

		private static function vrijednostKarte ($karta)
		{
			if (Tablic::$antilabeleBoja[$karta['BOJA']] == 'C' && Tablic::$antilabeleZnakova[$karta['ZNAK']] == '2')
				return 1;
			if (Tablic::$antilabeleBoja[$karta['BOJA']] == 'D' && Tablic::$antilabeleZnakova[$karta['ZNAK']] == '10')
				return 2;
			if (Tablic::$antilabeleZnakova[$karta['ZNAK']] == 'A' || Tablic::$antilabeleZnakova[$karta['ZNAK']] == '10' || Tablic::$antilabeleZnakova[$karta['ZNAK']] == 'J' || Tablic::$antilabeleZnakova[$karta['ZNAK']] == 'Q' || Tablic::$antilabeleZnakova[$karta['ZNAK']] == 'K')
				return 1;

			return 0;
		}

		private static function vrijednostSkupaKarata ($karte)
		{
			$v = 0;

			foreach ($karte as $karta)
				$v += Tablic::vrijednostKarte($karta);

			return $v;
		}

		private static function suma ($karte)
		{
			if (empty($karte))
				return array();

			$S = array(array_pop($karte)['ZNAK']);

			foreach ($karte as $karta)
			{
				$T = array();
				foreach ($S as $x)
				{
					$y = $karta['ZNAK'];
					if ($y === 1)
					{
						if ($x + 11 <= 14)
						{
							array_push($T, $x + 1);
							array_push($T, $x + 11);
						}
						else if ($x + 1 <= 14)
							array_push($T, $x + 1);
					}
					else if ($x + $y <= 14)
						array_push($T, $x + $y);
				}
				if (empty($T))
					return array();
				$S = $T;
			}

			foreach ($S as &$x)
				if ($x === 11)
					$x = 1;

			return array_unique($S);
		}

		private static function moguciPotezi ($karte)
		{
			$M = array();
			$P = partitivniSkup($karte);

			foreach ($P as $A)
			{
				$X = Tablic::suma($A);
				while (!empty($X))
				{
					$x = array_pop($X);
					if (array_key_exists($x, $M))
						array_push($M[$x], $A);
					else
						$M[$x] = array($A);
				}
			}

			return $M;
		}

		private static function noviSpil ()
		{
			$spil = array();
			foreach (Tablic::$labeleBoja as $boja)
				foreach (Tablic::$labeleZnakova as $znak)
					array_push($spil, array('BOJA' => $boja, 'ZNAK' => $znak));
			shuffle($spil);

			return $spil;
		}

		public static function inicijalizirajOkruzenje ()
		{
			Tablic::$db = new PDO('mysql:host=rp2.studenti.math.hr;dbname=milicevic;charset=utf8', 'student', 'pass.mysql');
		}

		public static function novaIgra ($n)
		{
			$sth = Tablic::$db->prepare('SELECT COUNT(*) AS BROJ FROM IGRA WHERE ID = :id;');
			$sth->bindParam(':id', $id);
			for ($i = 0; $i < 10000; ++$i)
			{
				$id = rand(0, 999);
				$sth->execute();
				$broj = $sth->fetch();
				if ((int)$broj['BROJ'] === 0)
					break;
			}
			if ($i === 10000)
				return NULL;
			$sth = Tablic::$db->prepare('INSERT INTO IGRA SET ID = ' . $id . ', BROJ_IGRACA = ' . ((int)$n - 1) . ';');
			$sth->execute();

			return $id;
		}

		public static function otvoreneIgre ()
		{
			$sth = Tablic::$db->prepare('SELECT ID, BROJ_IGRACA FROM IGRA;');
			$sth->execute();
			$igre = $sth->fetchAll();
			$sth = Tablic::$db->prepare('SELECT COUNT(*) AS BROJ FROM IGRAC WHERE ID_IGRE = :id;');
			$sth->bindParam(':id', $id);
			$otvorene = array();
			while (!empty($igre))
			{
				$igra = array_shift($igre);
				$id = $igra['ID'];
				$sth->execute();
				$broj = $sth->fetch();
				if ((int)$broj['BROJ'] < (int)$igra['BROJ_IGRACA'])
					array_push($otvorene, $id);
			}

			return $otvorene;
		}

		public function __construct ($id)
		{
			$this->id = $id;
		}

		private function brojIgraca ()
		{
			$sth = Tablic::$db->prepare('SELECT BROJ_IGRACA FROM IGRA WHERE ID = ' . $this->id . ';');
			$sth->execute();
			$broj = $sth->fetch();

			return (int)$broj['BROJ_IGRACA'];
		}

		private function ukupnoIgraca ()
		{
			$sth = Tablic::$db->prepare('SELECT COUNT(*) AS BROJ FROM IGRAC WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$broj = $sth->fetch();

			return (int)$broj['BROJ'];
		}

		private function rasporedIgraca ($igrac)
		{
			$raspored = array_fill(0, 4, NULL);

			$sth = Tablic::$db->prepare('SELECT ID FROM IGRAC WHERE ID_IGRE = ' . $this->id . ' ORDER BY REDNI_BROJ;');
			$sth->execute();
			$igraci = $sth->fetchAll();
			for ($i = 0; $i < count($igraci); ++$i)
			{
				if ($igraci[0]['ID'] == $igrac)
					break;
				array_push($igraci, array_shift($igraci));
			}
			if ($igraci[0]['ID'] == $igrac)
			{
				if (count($igraci) === 2)
				{
					$raspored[0] = $igraci[0]['ID'];
					$raspored[2] = $igraci[1]['ID'];
				}
				else if (count($igraci) === 3)
				{
					$raspored[0] = $igraci[0]['ID'];
					$raspored[1] = $igraci[1]['ID'];
					$raspored[3] = $igraci[2]['ID'];
				}
				else if (count($igraci) === 4)
				{
					$raspored[0] = $igraci[0]['ID'];
					$raspored[1] = $igraci[1]['ID'];
					$raspored[2] = $igraci[2]['ID'];
					$raspored[3] = $igraci[3]['ID'];
				}
			}

			return $raspored;
		}

		private function stol ()
		{
			$stol = array();

			foreach (Tablic::$db->query('SELECT BOJA, ZNAK FROM STOL WHERE ID_IGRE = ' . $this->id . ' ORDER BY ZNAK DESC, BOJA ASC;') as $karta)
				array_push($stol, array('BOJA' => Tablic::$antilabeleBoja[$karta['BOJA']], 'ZNAK' => Tablic::$antilabeleZnakova[$karta['ZNAK']]));

			return $stol;
		}

		private function otkrivenaRuka ($igrac)
		{
			$ruka = array();

			foreach (Tablic::$db->query('SELECT BOJA, ZNAK FROM RUKA WHERE ID_IGRACA = ' . $igrac . ' ORDER BY ZNAK DESC, BOJA ASC;') as $karta)
				array_push($ruka, array('BOJA' => Tablic::$antilabeleBoja[$karta['BOJA']], 'ZNAK' => Tablic::$antilabeleZnakova[$karta['ZNAK']]));

			return $ruka;
		}

		private function skrivenaRuka ($igrac)
		{
			$sth = Tablic::$db->prepare('SELECT COUNT(*) AS BROJ FROM RUKA WHERE ID_IGRACA = ' . $igrac . ';');
			$sth->execute();
			$broj = $sth->fetch();

			if ((int)$broj['BROJ'] === 0)
				return array();

			return array_fill(0, $broj['BROJ'], array('BOJA' => Tablic::$nedefinirano, 'ZNAK' => Tablic::$nedefinirano));
		}

		private function trenutniPotez ()
		{
			$potez = array('ime' => '', 'karta' => '');

			$sth = Tablic::$db->prepare('SELECT ID_IGRACA, BOJA, ZNAK FROM TRENUTNI_POTEZ WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$x = $sth->fetchAll();

			if (count($x) !== 0)
			{
				$sth = Tablic::$db->prepare('SELECT IME FROM IGRAC WHERE ID = ' . $x[0]['ID_IGRACA'] . ';');
				$sth->execute();
				$igrac = $sth->fetch();
				$potez['ime'] = $igrac['IME'];
				$potez['karta'] = array('BOJA' => Tablic::$antilabeleBoja[$x[0]['BOJA']], 'ZNAK' => Tablic::$antilabeleZnakova[$x[0]['ZNAK']]);
			}

			return $potez;
		}

		private function gotovo ()
		{
			$sth = Tablic::$db->prepare('SELECT GOTOVO FROM IGRA WHERE ID = ' . $this->id . ';');
			$sth->execute();
			$gotovo = $sth->fetch();

			return (bool)$gotovo['GOTOVO'];
		}

		private function rezultati ($igrac)
		{
			$rezultati = array();
			$raspored = $this->rasporedIgraca($igrac);

			foreach ($raspored as $i => $id)
			{
				if (is_null($id))
					$rezultati[$i + 1] = array('ime' => '', 'rezultat' => '', 'skupio' => '');
				else
					$rezultati[$i + 1] = array('ime' => $this->ime($id), 'rezultat' => $this->rezultat($id), 'skupio' => $this->skupio($id));
			}

			return $rezultati;
		}

		private function igraci ()
		{
			$sth = Tablic::$db->prepare('SELECT ID, IME, REDNI_BROJ, SKUPIO, REZULTAT FROM IGRAC WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$igraci = $sth->fetchAll();

			foreach ($igraci as &$igrac)
				$igrac = array_diff_key($igrac, array(0 => '', 1 => '', 2 => '', 3 => '', 4 => ''));

			return $igraci;
		}

		private function ime ($igrac)
		{
			$sth = Tablic::$db->prepare('SELECT IME FROM IGRAC WHERE ID = ' . $igrac . ';');
			$sth->execute();
			$ime = $sth->fetch();

			return $ime['IME'];
		}

		private function redniBroj ($igrac)
		{
			$sth = Tablic::$db->prepare('SELECT REDNI_BROJ FROM IGRAC WHERE ID = ' . $igrac . ';');
			$sth->execute();
			$i = $sth->fetch();

			return (int)$i['REDNI_BROJ'];
		}

		private function rezultat ($igrac)
		{
			$sth = Tablic::$db->prepare('SELECT REZULTAT FROM IGRAC WHERE ID = ' . $igrac . ';');
			$sth->execute();
			$rezultat = $sth->fetch();

			return (int)$rezultat['REZULTAT'];
		}

		private function skupio ($igrac)
		{
			$sth = Tablic::$db->prepare('SELECT SKUPIO FROM IGRAC WHERE ID = ' . $igrac . ';');
			$sth->execute();
			$skupio = $sth->fetch();

			return (int)$skupio['SKUPIO'];
		}

		private function sljedeci ()
		{
			$sth = Tablic::$db->prepare('SELECT ID_IGRACA AS ID FROM SLJEDECI WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$sljedeci = $sth->fetch();

			return $sljedeci['ID'];
		}

		private function zadnji ()
		{
			$sth = Tablic::$db->prepare('SELECT ID_IGRACA AS ID FROM ZADNJI WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$zadnji = $sth->fetchAll();
			if (empty($zadnji))
				return NULL;
			$igrac = $zadnji[0];

			return $igrac['ID'];
		}

		private function neprazanSpil ()
		{
			$sth = Tablic::$db->prepare('SELECT COUNT(*) AS BROJ FROM SPIL WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$broj = $sth->fetch();

			if ((int)$broj['BROJ'] === 0)
				return FALSE;

			return TRUE;
		}

		private function prazanStol ()
		{
			$sth = Tablic::$db->prepare('SELECT COUNT(*) AS BROJ FROM STOL WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$broj = $sth->fetch();
			if ((int)$broj['BROJ'] === 0)
				return TRUE;

			return FALSE;
		}

		private function trebaDijeliti ()
		{
			$sljedeci = $this->sljedeci();
			$sth = Tablic::$db->prepare('SELECT COUNT(*) AS BROJ FROM RUKA WHERE ID_IGRACA = ' . $sljedeci . ';');
			$sth->execute();
			$broj = $sth->fetch();

			if ((int)$broj['BROJ'] === 0)
				return TRUE;

			return FALSE;
		}

		private function naRedu ($igrac)
		{
			$sth = Tablic::$db->prepare('SELECT ID_IGRACA FROM SLJEDECI WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$sljedeci = $sth->fetch();
			if ($igrac != $sljedeci['ID_IGRACA'])
				return FALSE;

			return TRUE;
		}

		private function sadrziURuci ($igrac, $karta)
		{
			$sth = Tablic::$db->prepare('SELECT COUNT(*) AS BROJ FROM RUKA WHERE BOJA = ' . $karta['BOJA'] . ' AND ZNAK = ' . $karta['ZNAK'] . ' AND ID_IGRACA = ' . $igrac . ';');
			$sth->execute();
			$broj = $sth->fetch();

			if ((int)$broj['BROJ'] === 0)
				return FALSE;

			return TRUE;
		}

		private function podskupStola ($S)
		{
			$sth = Tablic::$db->prepare('SELECT * FROM STOL WHERE BOJA = :boja AND ZNAK = :znak AND ID_IGRE = ' . $this->id . ';');
			$sth->bindParam(':boja', $boja);
			$sth->bindParam(':znak', $znak);
			foreach ($S as $x)
			{
				$boja = $x['BOJA'];
				$znak = $x['ZNAK'];
				$sth->execute();
				if (empty($sth->fetchAll()))
					return FALSE;
			}

			return TRUE;
		}

		private function legalanPotez ($igrac, $karta, $S)
		{
			if (!$this->sadrziURuci($igrac, $karta))
				return FALSE;
			if (!$this->podskupStola($S))
				return FALSE;
			if (empty($S))
				return TRUE;

			$M = Tablic::moguciPotezi($S);
			if (array_key_exists($karta['ZNAK'], $M))
				$U = unijeDisjunktnih($M[$karta['ZNAK']]);
			else
				$U = array();

			while (!empty($U))
				if (jednakiSkupovi($S, array_pop($U)))
					return TRUE;

			return FALSE;
		}

		private function javiNelegalniPotez ()
		{
			return array('greska' => 'Potez je nelegalan');
		}

		private function objaviVrijeme ()
		{
			$sth = Tablic::$db->prepare('UPDATE IGRA SET ZADNJA_PROMJENA = CURRENT_TIMESTAMP WHERE ID = ' . $this->id . ';');
			$sth->execute();
			$sth = Tablic::$db->prepare('SELECT ZADNJA_PROMJENA FROM IGRA WHERE ID = ' . $this->id . ';');
			$sth->execute();
			$promjena = $sth->fetch();
			$_SESSION['vrijeme'] = $promjena['ZADNJA_PROMJENA'];
		}

		private function cekajNovoVrijeme ()
		{
			while (TRUE)
			{
				$sth = Tablic::$db->prepare("SELECT ZADNJA_PROMJENA FROM IGRA WHERE ZADNJA_PROMJENA > '" . $_SESSION['vrijeme'] . "' AND ID = " . $this->id . ";");
				$sth->execute();
				$promjena = $sth->fetchAll();
				if (!(empty($promjena)))
					break;
			}
			$_SESSION['vrijeme'] = $promjena[0]['ZADNJA_PROMJENA'];
		}

		private function postaviPrvog ()
		{
			$sth = Tablic::$db->prepare('SELECT ID FROM IGRAC WHERE REDNI_BROJ = 1 AND ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$prvi = $sth->fetch();

			$sth = Tablic::$db->prepare('INSERT INTO SLJEDECI SET ID_IGRE = ' . $this->id . ', ID_IGRACA = ' . $prvi['ID'] . ';');
			$sth->execute();
		}

		private function postaviPokrenuto ()
		{
			$sth = Tablic::$db->prepare('UPDATE IGRA SET POKRENUTO = TRUE WHERE ID = ' . $this->id . ';');
			$sth->execute();
		}

		private function azurirajSljedeceg ()
		{
			$sth = Tablic::$db->prepare('SELECT ID FROM IGRAC WHERE ID_IGRE = ' . $this->id . ' ORDER BY REDNI_BROJ;');
			$sth->execute();
			$igraci = $sth->fetchAll();
			$sth = Tablic::$db->prepare('SELECT REDNI_BROJ FROM IGRAC, SLJEDECI WHERE IGRAC.ID = SLJEDECI.ID_IGRACA AND SLJEDECI.ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$i = $sth->fetch();

			$sth = Tablic::$db->prepare('UPDATE SLJEDECI SET ID_IGRACA = ' . $igraci[$i['REDNI_BROJ'] % count($igraci)]['ID'] . ' WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
		}

		private function postaviZadnjeg ($igrac)
		{
			$sth = Tablic::$db->prepare('SELECT COUNT(*) AS BROJ FROM ZADNJI WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$broj = $sth->fetch();
			if ((int)$broj['BROJ'] === 0)
				$sth = Tablic::$db->prepare('INSERT INTO ZADNJI SET ID_IGRE = ' . $this->id . ', ID_IGRACA = ' . $igrac . ';');
			else
				$sth = Tablic::$db->prepare('UPDATE ZADNJI SET ID_IGRACA = ' . $igrac . ' WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
		}

		private function azurirajPotez ($igrac, $karta)
		{
			$potez = $this->trenutniPotez();
			if ($potez['karta'] === '')
				$sth = Tablic::$db->prepare('INSERT INTO TRENUTNI_POTEZ SET ID_IGRE = ' . $this->id . ', ID_IGRACA = ' . $igrac . ', BOJA = ' . $karta['BOJA'] . ', ZNAK = ' . $karta['ZNAK'] . ';');
			else
				$sth = Tablic::$db->prepare('UPDATE TRENUTNI_POTEZ SET ID_IGRACA = ' . $igrac . ', BOJA = ' . $karta['BOJA'] . ', ZNAK = ' . $karta['ZNAK'] . ' WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
		}

		private function makniSljedeceg ()
		{
			$sth = Tablic::$db->prepare('DELETE FROM SLJEDECI WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
		}

		private function postaviGotovo ()
		{
			$sth = Tablic::$db->prepare('UPDATE IGRA SET GOTOVO = TRUE WHERE ID = ' . $this->id . ';');
			$sth->execute();
		}

		private function dodajBodove ($igrac, $bodovi)
		{
			$sth = Tablic::$db->prepare('SELECT REZULTAT FROM IGRAC WHERE ID = ' . $igrac . ';');
			$sth->execute();
			$rezultat = $sth->fetch();
			$sth = Tablic::$db->prepare('UPDATE IGRAC SET REZULTAT = ' . ((int)$rezultat['REZULTAT'] + $bodovi) . ' WHERE ID = ' . $igrac . ';');
			$sth->execute();
		}

		private function evaluirajTablu ($igrac)
		{
			$this->dodajBodove($igrac, 1);
		}

		private function evaluirajMaksimum ()
		{
			$sth = Tablic::$db->prepare('SELECT MAX(SKUPIO) AS M FROM IGRAC WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$M = $sth->fetch();
			$sth = Tablic::$db->prepare('SELECT ID FROM IGRAC WHERE SKUPIO = ' . $M['M'] . ' AND ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$igraci = $sth->fetchAll();

			if (count($igraci) === 1)
				$this->dodajBodove($igrac[0]['ID'], 1);
		}

		private function staviNaStol ($igrac, $karta)
		{
			$sth = Tablic::$db->prepare('INSERT INTO STOL SET BOJA = ' . $karta['BOJA'] . ', ZNAK = ' . $karta['ZNAK'] . ', ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$sth = Tablic::$db->prepare('DELETE FROM RUKA WHERE BOJA = ' . $karta['BOJA'] . ' AND ZNAK = ' . $karta['ZNAK'] . ' AND ID_IGRACA = ' . $igrac . ';');
			$sth->execute();
		}

		private function pokupiSaStola ($igrac, $karta, $S)
		{
			$sth = Tablic::$db->prepare('DELETE FROM RUKA WHERE BOJA = ' . $karta['BOJA'] . ' AND ZNAK = ' . $karta['ZNAK'] . ' AND ID_IGRACA = ' . $igrac . ';');
			$sth->execute();
			$sth = Tablic::$db->prepare('DELETE FROM STOL WHERE BOJA = :boja AND ZNAK = :znak AND ID_IGRE = ' . $this->id . ';');
			$sth->bindParam(':boja', $boja);
			$sth->bindParam(':znak', $znak);
			foreach ($S as $x)
			{
				$boja = $x['BOJA'];
				$znak = $x['ZNAK'];
				$sth->execute();
			}
			$sth = Tablic::$db->prepare('SELECT SKUPIO FROM IGRAC WHERE ID = ' . $igrac . ';');
			$sth->execute();
			$skupio = $sth->fetch();
			$sth = Tablic::$db->prepare('UPDATE IGRAC SET SKUPIO = ' . ((int)$skupio['SKUPIO'] + 1 + count($S)) . ' WHERE ID = ' . $igrac . ';');
			$sth->execute();
			$this->dodajBodove($igrac, Tablic::vrijednostKarte($karta) + Tablic::vrijednostSkupaKarata($S));
		}

		private function pocistiStol ()
		{
			$zadnji = $this->zadnji();
			if (is_null($zadnji))
				return;
			$sth = Tablic::$db->prepare('SELECT SKUPIO FROM IGRAC WHERE ID = ' . $zadnji . ';');
			$sth->execute();
			$skupio = $sth->fetch();
			$sth = Tablic::$db->prepare('SELECT COUNT(*) AS BROJ FROM STOL WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$broj = $sth->fetch();
			$sth = Tablic::$db->prepare('UPDATE IGRAC SET SKUPIO = ' . ((int)$skupio['SKUPIO'] + (int)$broj['BROJ']) . ' WHERE ID = ' . $zadnji . ';');
			$sth->execute();
			$sth = Tablic::$db->prepare('SELECT BOJA, ZNAK FROM STOL WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$this->dodajBodove($zadnji, Tablic::vrijednostSkupaKarata($sth->fetchAll()));
			$sth = Tablic::$db->prepare('DELETE FROM STOL WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
		}

		private function inicijalizirajSpil ()
		{
			$spil = Tablic::noviSpil();

			foreach ($spil as $karta)
			{
				$sth = Tablic::$db->prepare("INSERT INTO SPIL SET BOJA = '" . $karta['BOJA'] . "', ZNAK = '" . $karta['ZNAK'] . "', ID_IGRE = " . $this->id . ";");
				$sth->execute();
			}
		}

		private function inicijalizirajStol ()
		{
			for ($i = 0; $i < 4; ++$i)
			{
				$sth = Tablic::$db->prepare('SELECT BOJA, ZNAK FROM SPIL WHERE ID_IGRE = ' . $this->id . ';');
				$sth->execute();
				$spil = $sth->fetchAll();
				if (empty($spil))
					break;
				$j = array_rand($spil);
				$sth = Tablic::$db->prepare('INSERT INTO STOL SET BOJA = ' . $spil[$j]['BOJA'] . ', ZNAK = ' . $spil[$j]['ZNAK'] . ', ID_IGRE = ' . $this->id . ';');
				$sth->execute();
				$sth = Tablic::$db->prepare('DELETE FROM SPIL WHERE BOJA = ' . $spil[$j]['BOJA'] . ' AND ZNAK = ' . $spil[$j]['ZNAK'] . ' AND ID_IGRE = ' . $this->id . ';');
				$sth->execute();
			}
		}

		private function podijeliKarte ()
		{
			$sth = Tablic::$db->prepare('SELECT ID FROM IGRAC WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$igraci = $sth->fetchAll();
			$sth = Tablic::$db->prepare('SELECT BOJA, ZNAK FROM SPIL WHERE ID_IGRE = ' . $this->id . ';');
			$sth1 = Tablic::$db->prepare('INSERT INTO RUKA SET BOJA = :boja, ZNAK = :znak, ID_IGRACA = :id;');
			$sth1->bindParam(':boja', $boja);
			$sth1->bindParam(':znak', $znak);
			$sth1->bindParam(':id', $id);
			$sth2 = Tablic::$db->prepare('DELETE FROM SPIL WHERE BOJA = :boja AND ZNAK = :znak AND ID_IGRE = ' . $this->id . ';');
			$sth2->bindParam(':boja', $boja);
			$sth2->bindParam(':znak', $znak);
			for ($i = 0; $i < 6; ++$i)
			{
				foreach ($igraci as $igrac)
				{
					$sth->execute();
					$spil = $sth->fetchAll();
					if (empty($spil))
						break;
					$id = $igrac['ID'];
					$j = array_rand($spil);
					$boja = $spil[$j]['BOJA'];
					$znak = $spil[$j]['ZNAK'];
					$sth1->execute();
					$sth2->execute();
				}
			}
		}

		private function dohvatiPotez ($igrac)
		{
			$get_karta = json_decode($_GET['kartaURuci']);
			$get_S = json_decode($_GET['karteNaStolu']);

			$karta = array('BOJA' => Tablic::$labeleBoja[$get_karta->BOJA], 'ZNAK' => Tablic::$labeleZnakova[$get_karta->ZNAK]);
			$S = array();
			while (!empty($get_S))
			{
				$x = array_pop($get_S);
				array_push($S, array('BOJA' => Tablic::$labeleBoja[$x->BOJA], 'ZNAK' => Tablic::$labeleZnakova[$x->ZNAK]));
			}

			return array('karta' => $karta, 'S' => $S);
		}

		private function izvrsiPotez ($igrac, $karta, $S)
		{
			if (empty($S))
				$this->staviNaStol($igrac, $karta);
			else
				$this->pokupiSaStola($igrac, $karta, $S);
		}

		private function zavrsi ()
		{
			$this->pocistiStol();
			$this->evaluirajMaksimum();
			$this->makniSljedeceg();
			$this->postaviGotovo();
		}

		public function dohvatiID ()
		{
			return $this->id;
		}

		public function spremna ()
		{
			if ($this->brojIgraca() === $this->ukupnoIgraca())
				return TRUE;

			return FALSE;
		}

		public function pokrenuto ()
		{
			$sth = Tablic::$db->prepare('SELECT POKRENUTO FROM IGRA WHERE ID = ' . $this->id . ';');
			$sth->execute();
			$pokrenuto = $sth->fetch();

			return (bool)$pokrenuto['POKRENUTO'];
		}

		public function dohvatiVrijeme ()
		{
			$sth = Tablic::$db->prepare('SELECT ZADNJA_PROMJENA FROM IGRA WHERE ID = ' . $this->id . ';');
			$sth->execute();
			$vrijeme = $sth->fetch();

			return $vrijeme['ZADNJA_PROMJENA'];
		}

		public function unistiIgru ()
		{
			$igraci = $this->igraci();
			$sth = Tablic::$db->prepare('DELETE FROM RUKA WHERE ID_IGRACA = :id;');
			$sth->bindParam(':id', $id);
			foreach ($igraci as $igrac)
			{
				$id = $igrac['ID'];
				$sth->execute();
			}
			$sth = Tablic::$db->prepare('DELETE FROM STOL WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$sth = Tablic::$db->prepare('DELETE FROM SPIL WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$sth = Tablic::$db->prepare('DELETE FROM TRENUTNI_POTEZ WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$sth = Tablic::$db->prepare('DELETE FROM ZADNJI WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$sth = Tablic::$db->prepare('DELETE FROM SLJEDECI WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$sth = Tablic::$db->prepare('DELETE FROM IGRAC WHERE ID_IGRE = ' . $this->id . ';');
			$sth->execute();
			$sth = Tablic::$db->prepare('DELETE FROM IGRA WHERE ID = ' . $this->id . ';');
			$sth->execute();
		}

		public function dodajIgraca ($ime)
		{
			if ($this->spremna())
				return NULL;
			$sth = Tablic::$db->prepare('SELECT COUNT(*) AS BROJ FROM IGRAC WHERE ID = :id;');
			$sth->bindParam(':id', $id);
			$id = 7;
			for ($i = 0; $i < 10000; ++$i)
			{
				$id = rand(0, 999);
				$sth->execute();
				$broj = $sth->fetch();
				if ((int)$broj['BROJ'] === 0)
					break;
			}
			if ($i === 10000)
				return NULL;
			$sth = Tablic::$db->prepare("INSERT INTO IGRAC SET ID = " . $id . ", IME = '" . $ime . "', ID_IGRE = " . $this->id . ", REDNI_BROJ = " . ($this->ukupnoIgraca() + 1) . ";");
			$sth->execute();

			return $id;
		}

		public function pokreniPartiju ()
		{
			if (!$this->spremna())
				return FALSE;

			$this->inicijalizirajSpil();
			$this->inicijalizirajStol();
			$this->podijeliKarte();
			$this->postaviPrvog();
			$this->postaviPokrenuto();
			$this->objaviVrijeme();

			return TRUE;
		}

		public function prikaziStanje ($igrac)
		{
			$stanje = array();
			$raspored = $this->rasporedIgraca($igrac);

			$stanje['naRedu'] = $this->naRedu($igrac);
			$stanje['stol'] = $this->stol();
			$stanje['ruke'] = array();
			foreach ($raspored as $i => $id)
			{
				if (is_null($id))
					$stanje['ruke'][$i + 1] = array();
				else if ($id == $igrac)
					$stanje['ruke'][$i + 1] = $this->otkrivenaRuka($id);
				else
					$stanje['ruke'][$i + 1] = $this->skrivenaRuka($id);
			}
			$stanje['potez'] = $this->trenutniPotez();
			$stanje['rezultati'] = $this->rezultati($igrac);
			$stanje['gotovo'] = $this->gotovo();

			return $stanje;
		}

		public function sljedeceStanje ($igrac)
		{
			if ($this->naRedu($igrac))
			{
				$potez = $this->dohvatiPotez($igrac);
				$karta = $potez['karta'];
				$S = $potez['S'];
				if (!$this->legalanPotez($igrac, $karta, $S))
					return $this->javiNelegalniPotez();
				$this->izvrsiPotez($igrac, $karta, $S);
				$this->azurirajPotez($igrac, $karta);
				if (!empty($S))
					$this->postaviZadnjeg($igrac);
				if ($this->prazanStol())
					$this->evaluirajTablu($igrac);
				$this->azurirajSljedeceg();
				if ($this->trebaDijeliti())
				{
					if ($this->neprazanSpil())
						$this->podijeliKarte();
					else
						$this->zavrsi();
				}
				$this->objaviVrijeme();
			}
			else
				$this->cekajNovoVrijeme();

			return $this->prikaziStanje($igrac);
		}
	};

?>
