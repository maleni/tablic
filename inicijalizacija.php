<?php
include 'klasa.php';
session_start();
Tablic::inicijalizirajOkruzenje();
function sendJSONandExit( $message )
{
    // Kao izlaz skripte pošalji $message u JSON formatu i prekini izvođenje.
    header( 'Content-type:application/json;charset=utf-8' );
    echo json_encode( $message );
    flush();
    exit( 0 );
}


$ime = $_GET['ime'];
$broj = $_GET['broj'];
$_SESSION['ime'] = $ime;
$message = [];


if($broj === "")
{
   $igre = Tablic::otvoreneIgre();
   if(count($igre)==0)
   {
     $message['rez']= "prazna";
   }
   else{
     $_SESSION['igra']= new Tablic($igre[0]);
     $_SESSION['id'] = $_SESSION['igra']->dodajIgraca($_SESSION['ime']);
     if(is_null($_SESSION['id']))
        $message['rez'] = "greska";
     else {
      $_SESSION['admin'] = false;
      $message['rez']= "spajanje";
      $message['id'] = $_SESSION['igra']->dohvatiID();
     }
   }
}
else {
  $id = Tablic::novaIgra((int)$broj);
  if(is_null($id))
      $message['rez'] = "greska";
  else {
     $_SESSION['igra']= new Tablic($id);
     $_SESSION['id'] = $_SESSION['igra']->dodajIgraca($_SESSION['ime']);
     if(is_null($_SESSION['id']))
        $message['rez'] = "greska";
    else {
      $_SESSION['admin'] = true;
      $message['rez'] = "prazna";
      $message['id'] = $_SESSION['igra']->dohvatiID();
    }
  }

}

sendJSONandExit($message);
 ?>
