<?php
include 'klasa.php';
Tablic::inicijalizirajOkruzenje();
function sendJSONandExit( $message )
{
    // Kao izlaz skripte pošalji $message u JSON formatu i prekini izvođenje.
    header( 'Content-type:application/json;charset=utf-8' );
    echo json_encode( $message );
    flush();
    exit( 0 );
}

$id = $_GET['id'];
$igra = new Tablic($id);
while(!$igra->spremna());

$message['rez'] = "gotovo";

sendJSONandExit($message);

 ?>
